'use strict';

function GetBaseUrl(prtc, host, port) {
  var baseRoute = '';
  if (prtc.indexOf(':') === -1) prtc += ':';
  if (prtc) baseRoute = prtc.concat('//', baseRoute);
  if (host) baseRoute = baseRoute.concat(host);
  if (port) baseRoute = baseRoute.concat(':', port);

  return baseRoute;
}

module.exports = GetBaseUrl;
