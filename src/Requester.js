/* globals location */
'use strict';

var localStorage    = require('local-storage');
var RequesterHttp   = require('./RequesterHttp');
var RequesterSocket = require('./RequesterSocket');

function Requester() {
  var req = this;
  var http;
  var socket;

  var hostName     = location && location.hasOwnProperty('hostname') && location.hostname ? location.hostname : '127.0.0.1';
  var hostProtocol = location && location.hasOwnProperty('protocol') && location.protocol ? location.protocol : 'http:';
  var port         = location && location.hasOwnProperty('port')     && location.port     ? String(location.port) : '';

  if (port === '80') port = '';

  var get = {
    http: function() {
      if (!http)
        http = RequesterHttp.getInstance(hostProtocol, hostName, port);

      return http;
    },

    socket: function() {
      if (!socket)
        socket = RequesterSocket.getInstance(hostProtocol, hostName, port);

      return socket;
    }
  };

  req.getProtocol = function() {
    var connectionHttp = 'http';
    var connectionProtocol = localStorage('connectionProtocol');
    if (!connectionProtocol) connectionProtocol = connectionHttp;
    return get[connectionProtocol]();
  };
}

module.exports = Requester;
