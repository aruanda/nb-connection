'use strict';

var connection;
var Requester = require('./Requester');
var RequesterORM = require('./RequesterORM');

module.exports = {
  getInstance: function(path) {
    if (!path) path = '/api';
    if (!connection) connection = new Requester();
    return new RequesterORM(path, connection);
  }
};
