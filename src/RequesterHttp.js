'use strict';

var promise = require('q').Promise;
var request = require('superagent');
var getUrlPath = require('./util/GetUrlPath');

function RequesterHttp(prtc, host, port) {
  var http = this;

  http.request = function(verb, address, params, files) {
    verb    = verb    ? String(verb)    : 'get';
    address = address ? String(address) : '/';
    params  = params  ? params          : {};

    var url = getUrlPath(prtc, host, port, address);

    return promise(function(resolve, reject) {
      verb = verb.toLowerCase();

      var sendObj = request[verb](url).set('Accept', 'application/json');

      var withFiles = Boolean(files && files.hasOwnProperty('length') && files.length);

      if (withFiles) {
        files.filter(function(file) {
          return !file.hasOwnProperty('id');
        }).forEach(function(file) {
          file.originalname = file.name;
          sendObj.attach(file.field, file);
        });

        Object.keys(params).forEach(function(field) {
          var valField = params[field];
          var isArray = Array.isArray(valField);
          if (isArray) valField = JSON.stringify(valField);

          var isObject = (typeof valField) === 'object';
          if (isObject) valField = JSON.stringify(valField);

          if (params[field] !== undefined) sendObj.field(field, valField);
        });
      } else {
        if (verb === 'get')  sendObj.query({ q: JSON.stringify(params) });
        if (verb === 'post') sendObj.send(params);
        if (verb === 'put')  sendObj.send(params);
      }

      sendObj.end(function(err, res) {
        if (err) return reject(err);
        resolve(res);
      });
    });
  };
}

var httpInstance;
module.exports = {
  getInstance: function(prtc, host, port) {
    if (!httpInstance) httpInstance = new RequesterHttp(prtc, host, port);
    return httpInstance;
  }
};
